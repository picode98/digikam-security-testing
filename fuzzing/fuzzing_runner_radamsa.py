import pyautogui

from subprocess import check_call, Popen
from time import sleep
from glob import glob
import os
from concurrent.futures import ThreadPoolExecutor, wait, ALL_COMPLETED


def radamsa_generate_image(in_file_name, out_file_name, seed):
    print(f'Generating image for seed {seed}...')
    out_file = open(out_file_name, "wb")
    check_call(["radamsa", in_file_name, "-s", str(seed)], stdout=out_file)
    out_file.close()


batch_size = 500
input_file = "./temp/IMG_0085_small.jpg"
out_dir = "./temp"
valgrind_log_path = "./digikam_valgrind.log"

valgrind_log = open(valgrind_log_path, 'w')
digikam_proc = Popen(["../test_install/bin/digikam"], stdout=valgrind_log, stderr=valgrind_log)
input('Press ENTER when digiKam is finished opening:')

fuzz_seed = 1
while digikam_proc.poll() is None:
    for file in glob(f"{out_dir}/fuzz_*.jpg"):
        os.remove(file)
    executor = ThreadPoolExecutor(max_workers=4)
    futures = {executor.submit(lambda seed=seed: radamsa_generate_image(input_file, f"{out_dir}/fuzz_{seed}.jpg", seed))
               for seed in range(fuzz_seed, fuzz_seed + batch_size)}
    fuzz_seed += batch_size

    wait(futures, return_when=ALL_COMPLETED)

    pyautogui.press('f5')
    sleep(60.0)
    pyautogui.press('home')
    sleep(0.5)
    pyautogui.press('enter')
    sleep(0.5)

    for _ in range(batch_size + 10):
        pyautogui.press('right')
        sleep(0.4)
        
    pyautogui.press('esc')
    sleep(0.5)

valgrind_log.close()
