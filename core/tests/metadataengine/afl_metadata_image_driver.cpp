//
// Created by sdk on 11/17/21.
//

#include <QCoreApplication>
#include <QLoggingCategory>

#include "digikam_debug.h"
#include "metaenginesettingscontainer.h"
#include "dmetadata.h"

#include <iostream>
#include <fstream>
#include <QFile>

using namespace Digikam;

void copyFileOverwrite(const std::string& src, const std::string& dst)
{
    std::ifstream inputFile;
    std::ofstream outputFile;
    inputFile.exceptions(std::ios::failbit);
    outputFile.exceptions(std::ios::failbit);

    inputFile.open(src, std::ios::binary);
    outputFile.open(dst, std::ios::binary);

    outputFile << inputFile.rdbuf();
}

int main(int argc, char** argv)
{
    auto&& app = QCoreApplication(argc, argv);

    MetaEngineSettingsContainer writerSettings;
    writerSettings.metadataWritingMode = DMetadata::WRITE_TO_FILE_ONLY;

    assert(QCoreApplication::arguments().size() == 2);
    copyFileOverwrite(QCoreApplication::arguments()[1].toStdString(), "./test_out.jpg");

    QScopedPointer<DMetadata> metadata(new DMetadata);
    metadata->setSettings(writerSettings);
    QStringList newTags;
    assert(metadata->load(QLatin1String("./test_out.jpg")));

    const QString newCameraName = QString::fromLatin1("Some Camera Model");
    metadata->setExifTagString("Exif.Image.Model", newCameraName);

    long exposureNum = 1, exposureDenom = 3;
    if(!metadata->getExifTagRational("Exif.Photo.ExposureTime", exposureNum, exposureDenom))
    {
        qCDebug(DIGIKAM_TESTS_LOG) << "Did not find exposure value in source image";
    }

    assert(metadata->setExifTagRational("Exif.Photo.ExposureTime", exposureNum, exposureNum + exposureDenom));


    double startLat, startLng, startAlt;
    if(!metadata->getGPSInfo(startAlt, startLat, startLng))
    {
        startLat = -43.375; startLng = 0.0; startAlt = 620.75;
    }
    // qCDebug(DIGIKAM_TESTS_LOG) << "Lat: " << QString::number(startLat, 'g', std::numeric_limits<double>::digits10);
    assert(metadata->setGPSInfo(startAlt + 500.0, startLat * 2.0, startLng / 2.0));

    metadata->getItemTagsPath(newTags);

    std::vector<std::string> testTags = {"Tag 1", "A second tag", "A longer third tag"};

//    QMap<QString, QString> appliedChanges;
    // std::string line;
    for(const auto& thisTag : testTags)
    {
        auto qTag = QString::fromStdString(thisTag);
        newTags.append(qTag);
        // auto parts = qTag.split(QLatin1Char('='));

//        if(parts.size() == 2)
//        {
//            qCDebug(DIGIKAM_TESTS_LOG) << "Will set key "<< parts[0] << " to value " << parts[1];
//            assert(metadata->setExifTagString(parts[0].toStdString().c_str(), parts[1]));
//            assert(metadata->getExifTagString(parts[0].toStdString().c_str()) == parts[1]);
//            appliedChanges[parts[0]] = parts[1];
//        }
//        else
//        {
//            qCDebug(DIGIKAM_TESTS_LOG) << "Ignoring unknown argument: " << qTag;
//        }
    }
    assert(metadata->setItemTagsPath(newTags));

    assert(metadata->applyChanges());

    QScopedPointer<DMetadata> newMetadata(new DMetadata);
    assert(newMetadata->load(QLatin1String("./test_out.jpg")));

    assert(newMetadata->getExifTagString("Exif.Image.Model") == newCameraName);
    long newNum = 0, newDenom = 0;
    assert(newMetadata->getExifTagRational("Exif.Photo.ExposureTime", newNum, newDenom));
    assert(newNum == exposureNum);
    assert(newDenom == exposureNum + exposureDenom);

    double newAltitude, newLat, newLng;
    assert(newMetadata->getGPSAltitude(&newAltitude) && newAltitude == startAlt + 500.0);
    assert(newMetadata->getGPSLatitudeNumber(&newLat) && newLat == startLat * 2.0);
    assert(newMetadata->getGPSLongitudeNumber(&newLng) && newLng == startLng / 2.0);

//    for(const QString& key : appliedChanges.keys())
//    {
//        assert(newMetadata->getExifTagString(key.toStdString().c_str()) == appliedChanges[key]);
//    }

    QStringList readTags;
    newMetadata->getItemTagsPath(readTags);

    for(const QString& tag : newTags)
    {
        assert(readTags.contains(tag));
    }
}