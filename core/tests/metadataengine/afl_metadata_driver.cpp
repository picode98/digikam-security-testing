//
// Created by sdk on 11/17/21.
//

#include <QCoreApplication>
#include <QLoggingCategory>

#include "digikam_debug.h"
#include "metaenginesettingscontainer.h"
#include "dmetadata.h"

#include <iostream>
#include <fstream>
#include <QFile>

using namespace Digikam;

void copyFileOverwrite(const std::string& src, const std::string& dst)
{
    std::ifstream inputFile;
    std::ofstream outputFile;
    inputFile.exceptions(std::ios::failbit);
    outputFile.exceptions(std::ios::failbit);

    inputFile.open(src, std::ios::binary);
    outputFile.open(dst, std::ios::binary);

    outputFile << inputFile.rdbuf();
}

int main(int argc, char** argv)
{
    auto&& app = QCoreApplication(argc, argv);

    MetaEngineSettingsContainer writerSettings;
    writerSettings.metadataWritingMode = DMetadata::WRITE_TO_FILE_ONLY;

    copyFileOverwrite("./test.jpg", "./test_out.jpg");

    QScopedPointer<DMetadata> metadata(new DMetadata);
    metadata->setSettings(writerSettings);
    QStringList newTags;
    assert(metadata->load(QLatin1String("./test_out.jpg")));
    metadata->getItemTagsPath(newTags);

//    QMap<QString, QString> appliedChanges;
    std::string line;
    while(std::getline(std::cin, line))
    {
        auto qLine = QString::fromStdString(line);
        newTags.append(qLine);
        // auto parts = qLine.split(QLatin1Char('='));

//        if(parts.size() == 2)
//        {
//            qCDebug(DIGIKAM_TESTS_LOG) << "Will set key "<< parts[0] << " to value " << parts[1];
//            assert(metadata->setExifTagString(parts[0].toStdString().c_str(), parts[1]));
//            assert(metadata->getExifTagString(parts[0].toStdString().c_str()) == parts[1]);
//            appliedChanges[parts[0]] = parts[1];
//        }
//        else
//        {
//            qCDebug(DIGIKAM_TESTS_LOG) << "Ignoring unknown argument: " << qLine;
//        }
    }
    assert(metadata->setItemTagsPath(newTags));

    assert(metadata->applyChanges());

    QScopedPointer<DMetadata> newMetadata(new DMetadata);
    assert(newMetadata->load(QLatin1String("./test_out.jpg")));

//    for(const QString& key : appliedChanges.keys())
//    {
//        assert(newMetadata->getExifTagString(key.toStdString().c_str()) == appliedChanges[key]);
//    }

    QStringList readTags;
    newMetadata->getItemTagsPath(readTags);

    for(const QString& tag : newTags)
    {
        assert(readTags.contains(tag));
    }
}